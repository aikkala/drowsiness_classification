# CNN PSD Drowsiness Classification

Convolutional neural network for power spectral density based drowsiness classification

**DISCLAIMER** This is barely a proof-of-concept, not a properly tested or validated approach to drowsiness classification. If you need a drowsiness classification algorithm take a look at this [properly validated algorithm](https://www.sciencedirect.com/science/article/pii/S1053811918303562). 

## Data

- Dataset1 and Dataset2 were downloaded from <https://www.repository.cam.ac.uk/handle/1810/271847>. These files would be in folders [dataset1](https://gitlab.com/aikkala/sleep_classification/tree/master/dataset1) and [dataset2](https://gitlab.com/aikkala/sleep_classification/tree/master/dataset2), but they are omitted from this repository because of their size
- Dataset2 was used to train (and test) the CNN. Dataset1 was used to further validate the CNN.

## Preprocess
1. Dataset2 was preprocessed with [create_dataset2()](https://gitlab.com/aikkala/sleep_classification/blob/master/scripts/create_dataset1.m); dataset1 was preprocessed with [create_dataset1()](https://gitlab.com/aikkala/sleep_classification/blob/master/scripts/create_dataset2.m) (note that this script is missing two preprocessing steps, removal of bad channels and bad epochs)
2. Use script [create_training_sets()](https://gitlab.com/aikkala/sleep_classification/blob/master/scripts/create_training_sets.m) to organise the preprocessed data into a format that's easier to work with
    - Dataset2 is divided into five folds training and testing, dataset1 is used only for testing

## Train the model
Use [pipeline.py](https://gitlab.com/aikkala/sleep_classification/blob/master/scripts/pipeline.py) to train the model

## Use the model to predict on new data
- This script does not exist. You need to save the graph you train in [pipeline.py](https://gitlab.com/aikkala/sleep_classification/blob/master/scripts/pipeline.py) and use it to predict on new data.
- Note: there are [five trained models]() (trained with dataset2; one model for each train set of a 5-fold cross-validation) along with their prediction stats (recall, precision and f1). You should be able to use these models by loading them with `tf.saved_model.loader.load(...)` function. Something like this should work (adapted from [this stackoverflow answer](https://stackoverflow.com/a/50852627)):

```
from tensorflow.python.saved_model import tag_constants

graph = tf.Graph()
with restored_graph.as_default():
    with tf.Session as sess:
        tf.saved_model.loader.load(sess, [tag_constants.SERVING], 'saved_models')
        test_prediction = graph.get_tensor_by_name('Softmax_1:0')
        tf_test_dataset = graph.get_tensor_by_name('test_data_ph:0')

        predicted_labels_softmax = sess.run(test_prediction, feed_dict={tf_test_datast: *your_data_here*})
```


