øÕ
ū'Ņ'
:
Add
x"T
y"T
z"T"
Ttype:
2	

ApplyGradientDescent
var"T

alpha"T

delta"T
out"T" 
Ttype:
2	"
use_lockingbool( 
x
Assign
ref"T

value"T

output_ref"T"	
Ttype"
validate_shapebool("
use_lockingbool(
s
	AssignAdd
ref"T

value"T

output_ref"T" 
Ttype:
2	"
use_lockingbool( 
R
BroadcastGradientArgs
s0"T
s1"T
r0"T
r1"T"
Ttype0:
2	
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
ģ
Conv2D

input"T
filter"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)


Conv2DBackpropFilter

input"T
filter_sizes
out_backprop"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)


Conv2DBackpropInput
input_sizes
filter"T
out_backprop"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

W

ExpandDims

input"T
dim"Tdim
output"T"	
Ttype"
Tdimtype0:
2	
^
Fill
dims"
index_type

value"T
output"T"	
Ttype"

index_typetype0:
2	
,
Floor
x"T
y"T"
Ttype:
2
.
Identity

input"T
output"T"	
Ttype
?

LogSoftmax
logits"T

logsoftmax"T"
Ttype:
2
p
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:
	2
Ō
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C
ī
MaxPoolGrad

orig_input"T
orig_output"T	
grad"T
output"T"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID"-
data_formatstringNHWC:
NHWCNCHW"
Ttype0:
2	

Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(
=
Mul
x"T
y"T
z"T"
Ttype:
2	
.
Neg
x"T
y"T"
Ttype:

2	

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
6
Pow
x"T
y"T
z"T"
Ttype:

2	
>
RealDiv
x"T
y"T
z"T"
Ttype:
2	
E
Relu
features"T
activations"T"
Ttype:
2	
V
ReluGrad
	gradients"T
features"T
	backprops"T"
Ttype:
2	
[
Reshape
tensor"T
shape"Tshape
output"T"	
Ttype"
Tshapetype0:
2	
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
e
ShapeN
input"T*N
output"out_type*N"
Nint(0"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
a
Slice

input"T
begin"Index
size"Index
output"T"	
Ttype"
Indextype:
2	
9
Softmax
logits"T
softmax"T"
Ttype:
2
j
SoftmaxCrossEntropyWithLogits
features"T
labels"T	
loss"T
backprop"T"
Ttype:
2
2
StopGradient

input"T
output"T"	
Ttype
ö
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
:
Sub
x"T
y"T
z"T"
Ttype:
2	

Sum

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
c
Tile

input"T
	multiples"
Tmultiples
output"T"	
Ttype"

Tmultiplestype0:
2	

TruncatedNormal

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
s

VariableV2
ref"dtype"
shapeshape"
dtypetype"
	containerstring "
shared_namestring 
&
	ZerosLike
x"T
y"T"	
Ttype"serve*1.12.02v1.12.0-0-ga6d8ffae09Ü
n
train_data_phPlaceholder*
dtype0*&
_output_shapes
:C*
shape:C
`
train_labels_phPlaceholder*
dtype0*
_output_shapes

:*
shape
:

test_data_phPlaceholder*$
shape:’’’’’’’’’C*
dtype0*/
_output_shapes
:’’’’’’’’’C
q
test_labels_phPlaceholder*
dtype0*'
_output_shapes
:’’’’’’’’’*
shape:’’’’’’’’’
o
truncated_normal/shapeConst*%
valueB"            *
dtype0*
_output_shapes
:
Z
truncated_normal/meanConst*
dtype0*
_output_shapes
: *
valueB
 *    
\
truncated_normal/stddevConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 
¢
 truncated_normal/TruncatedNormalTruncatedNormaltruncated_normal/shape*

seed *
T0*
dtype0*
seed2 *&
_output_shapes
:

truncated_normal/mulMul truncated_normal/TruncatedNormaltruncated_normal/stddev*
T0*&
_output_shapes
:
u
truncated_normalAddtruncated_normal/multruncated_normal/mean*
T0*&
_output_shapes
:

Variable
VariableV2*
dtype0*
	container *&
_output_shapes
:*
shape:*
shared_name 
¬
Variable/AssignAssignVariabletruncated_normal*
validate_shape(*&
_output_shapes
:*
use_locking(*
T0*
_class
loc:@Variable
q
Variable/readIdentityVariable*
T0*
_class
loc:@Variable*&
_output_shapes
:
b
truncated_normal_1/shapeConst*
dtype0*
_output_shapes
:*
valueB:
\
truncated_normal_1/meanConst*
valueB
 *    *
dtype0*
_output_shapes
: 
^
truncated_normal_1/stddevConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 

"truncated_normal_1/TruncatedNormalTruncatedNormaltruncated_normal_1/shape*
T0*
dtype0*
seed2 *
_output_shapes
:*

seed 

truncated_normal_1/mulMul"truncated_normal_1/TruncatedNormaltruncated_normal_1/stddev*
T0*
_output_shapes
:
o
truncated_normal_1Addtruncated_normal_1/multruncated_normal_1/mean*
_output_shapes
:*
T0
v

Variable_1
VariableV2*
dtype0*
	container *
_output_shapes
:*
shape:*
shared_name 
Ø
Variable_1/AssignAssign
Variable_1truncated_normal_1*
use_locking(*
T0*
_class
loc:@Variable_1*
validate_shape(*
_output_shapes
:
k
Variable_1/readIdentity
Variable_1*
T0*
_class
loc:@Variable_1*
_output_shapes
:
q
truncated_normal_2/shapeConst*%
valueB"            *
dtype0*
_output_shapes
:
\
truncated_normal_2/meanConst*
valueB
 *    *
dtype0*
_output_shapes
: 
^
truncated_normal_2/stddevConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 
¦
"truncated_normal_2/TruncatedNormalTruncatedNormaltruncated_normal_2/shape*
dtype0*
seed2 *&
_output_shapes
:*

seed *
T0

truncated_normal_2/mulMul"truncated_normal_2/TruncatedNormaltruncated_normal_2/stddev*
T0*&
_output_shapes
:
{
truncated_normal_2Addtruncated_normal_2/multruncated_normal_2/mean*
T0*&
_output_shapes
:


Variable_2
VariableV2*
dtype0*
	container *&
_output_shapes
:*
shape:*
shared_name 
“
Variable_2/AssignAssign
Variable_2truncated_normal_2*
validate_shape(*&
_output_shapes
:*
use_locking(*
T0*
_class
loc:@Variable_2
w
Variable_2/readIdentity
Variable_2*&
_output_shapes
:*
T0*
_class
loc:@Variable_2
b
truncated_normal_3/shapeConst*
valueB:*
dtype0*
_output_shapes
:
\
truncated_normal_3/meanConst*
dtype0*
_output_shapes
: *
valueB
 *    
^
truncated_normal_3/stddevConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 

"truncated_normal_3/TruncatedNormalTruncatedNormaltruncated_normal_3/shape*
dtype0*
seed2 *
_output_shapes
:*

seed *
T0

truncated_normal_3/mulMul"truncated_normal_3/TruncatedNormaltruncated_normal_3/stddev*
T0*
_output_shapes
:
o
truncated_normal_3Addtruncated_normal_3/multruncated_normal_3/mean*
T0*
_output_shapes
:
v

Variable_3
VariableV2*
shared_name *
dtype0*
	container *
_output_shapes
:*
shape:
Ø
Variable_3/AssignAssign
Variable_3truncated_normal_3*
use_locking(*
T0*
_class
loc:@Variable_3*
validate_shape(*
_output_shapes
:
k
Variable_3/readIdentity
Variable_3*
T0*
_class
loc:@Variable_3*
_output_shapes
:
i
truncated_normal_4/shapeConst*
valueB"       *
dtype0*
_output_shapes
:
\
truncated_normal_4/meanConst*
dtype0*
_output_shapes
: *
valueB
 *    
^
truncated_normal_4/stddevConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 

"truncated_normal_4/TruncatedNormalTruncatedNormaltruncated_normal_4/shape*
T0*
dtype0*
seed2 *
_output_shapes
:	 *

seed 

truncated_normal_4/mulMul"truncated_normal_4/TruncatedNormaltruncated_normal_4/stddev*
_output_shapes
:	 *
T0
t
truncated_normal_4Addtruncated_normal_4/multruncated_normal_4/mean*
T0*
_output_shapes
:	 


Variable_4
VariableV2*
dtype0*
	container *
_output_shapes
:	 *
shape:	 *
shared_name 
­
Variable_4/AssignAssign
Variable_4truncated_normal_4*
use_locking(*
T0*
_class
loc:@Variable_4*
validate_shape(*
_output_shapes
:	 
p
Variable_4/readIdentity
Variable_4*
T0*
_class
loc:@Variable_4*
_output_shapes
:	 
b
truncated_normal_5/shapeConst*
dtype0*
_output_shapes
:*
valueB: 
\
truncated_normal_5/meanConst*
valueB
 *    *
dtype0*
_output_shapes
: 
^
truncated_normal_5/stddevConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 

"truncated_normal_5/TruncatedNormalTruncatedNormaltruncated_normal_5/shape*
dtype0*
seed2 *
_output_shapes
: *

seed *
T0

truncated_normal_5/mulMul"truncated_normal_5/TruncatedNormaltruncated_normal_5/stddev*
_output_shapes
: *
T0
o
truncated_normal_5Addtruncated_normal_5/multruncated_normal_5/mean*
T0*
_output_shapes
: 
v

Variable_5
VariableV2*
shared_name *
dtype0*
	container *
_output_shapes
: *
shape: 
Ø
Variable_5/AssignAssign
Variable_5truncated_normal_5*
use_locking(*
T0*
_class
loc:@Variable_5*
validate_shape(*
_output_shapes
: 
k
Variable_5/readIdentity
Variable_5*
T0*
_class
loc:@Variable_5*
_output_shapes
: 
i
truncated_normal_6/shapeConst*
valueB"       *
dtype0*
_output_shapes
:
\
truncated_normal_6/meanConst*
valueB
 *    *
dtype0*
_output_shapes
: 
^
truncated_normal_6/stddevConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 

"truncated_normal_6/TruncatedNormalTruncatedNormaltruncated_normal_6/shape*
T0*
dtype0*
seed2 *
_output_shapes

: *

seed 

truncated_normal_6/mulMul"truncated_normal_6/TruncatedNormaltruncated_normal_6/stddev*
T0*
_output_shapes

: 
s
truncated_normal_6Addtruncated_normal_6/multruncated_normal_6/mean*
T0*
_output_shapes

: 
~

Variable_6
VariableV2*
shared_name *
dtype0*
	container *
_output_shapes

: *
shape
: 
¬
Variable_6/AssignAssign
Variable_6truncated_normal_6*
T0*
_class
loc:@Variable_6*
validate_shape(*
_output_shapes

: *
use_locking(
o
Variable_6/readIdentity
Variable_6*
T0*
_class
loc:@Variable_6*
_output_shapes

: 
b
truncated_normal_7/shapeConst*
dtype0*
_output_shapes
:*
valueB:
\
truncated_normal_7/meanConst*
valueB
 *    *
dtype0*
_output_shapes
: 
^
truncated_normal_7/stddevConst*
valueB
 *ĶĢĢ=*
dtype0*
_output_shapes
: 

"truncated_normal_7/TruncatedNormalTruncatedNormaltruncated_normal_7/shape*
T0*
dtype0*
seed2 *
_output_shapes
:*

seed 

truncated_normal_7/mulMul"truncated_normal_7/TruncatedNormaltruncated_normal_7/stddev*
T0*
_output_shapes
:
o
truncated_normal_7Addtruncated_normal_7/multruncated_normal_7/mean*
T0*
_output_shapes
:
v

Variable_7
VariableV2*
shared_name *
dtype0*
	container *
_output_shapes
:*
shape:
Ø
Variable_7/AssignAssign
Variable_7truncated_normal_7*
use_locking(*
T0*
_class
loc:@Variable_7*
validate_shape(*
_output_shapes
:
k
Variable_7/readIdentity
Variable_7*
_output_shapes
:*
T0*
_class
loc:@Variable_7
Ķ
Conv2DConv2Dtrain_data_phVariable/read*
	dilations
*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingSAME*&
_output_shapes
:C

MaxPoolMaxPoolConv2D*
ksize
*
paddingSAME*&
_output_shapes
:"*
T0*
data_formatNHWC*
strides

U
addAddMaxPoolVariable_1/read*&
_output_shapes
:"*
T0
B
ReluReluadd*
T0*&
_output_shapes
:"
Č
Conv2D_1Conv2DReluVariable_2/read*
	dilations
*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingSAME*&
_output_shapes
:"
”
	MaxPool_1MaxPoolConv2D_1*
strides
*
data_formatNHWC*
ksize
*
paddingSAME*&
_output_shapes
:*
T0
Y
add_1Add	MaxPool_1Variable_3/read*
T0*&
_output_shapes
:
F
Relu_1Reluadd_1*
T0*&
_output_shapes
:
^
ShapeConst*%
valueB"            *
dtype0*
_output_shapes
:
]
strided_slice/stackConst*
valueB: *
dtype0*
_output_shapes
:
_
strided_slice/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
_
strided_slice/stack_2Const*
valueB:*
dtype0*
_output_shapes
:
ł
strided_sliceStridedSliceShapestrided_slice/stackstrided_slice/stack_1strided_slice/stack_2*
Index0*
T0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: 
_
strided_slice_1/stackConst*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_1/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_1/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

strided_slice_1StridedSliceShapestrided_slice_1/stackstrided_slice_1/stack_1strided_slice_1/stack_2*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask
_
strided_slice_2/stackConst*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_2/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_2/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

strided_slice_2StridedSliceShapestrided_slice_2/stackstrided_slice_2/stack_1strided_slice_2/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0
M
mulMulstrided_slice_1strided_slice_2*
_output_shapes
: *
T0
_
strided_slice_3/stackConst*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_3/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_3/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

strided_slice_3StridedSliceShapestrided_slice_3/stackstrided_slice_3/stack_1strided_slice_3/stack_2*
end_mask *
_output_shapes
: *
Index0*
T0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask 
C
mul_1Mulmulstrided_slice_3*
T0*
_output_shapes
: 
e
Reshape/shapePackstrided_slicemul_1*
T0*

axis *
N*
_output_shapes
:
a
ReshapeReshapeRelu_1Reshape/shape*
T0*
Tshape0*
_output_shapes
:	
y
MatMulMatMulReshapeVariable_4/read*
transpose_b( *
T0*
transpose_a( *
_output_shapes

: 
N
add_2AddMatMulVariable_5/read*
T0*
_output_shapes

: 
>
Relu_2Reluadd_2*
T0*
_output_shapes

: 
z
MatMul_1MatMulRelu_2Variable_6/read*
transpose_a( *
_output_shapes

:*
transpose_b( *
T0
P
add_3AddMatMul_1Variable_7/read*
T0*
_output_shapes

:

9softmax_cross_entropy_with_logits_sg/labels_stop_gradientStopGradienttrain_labels_ph*
T0*
_output_shapes

:
k
)softmax_cross_entropy_with_logits_sg/RankConst*
dtype0*
_output_shapes
: *
value	B :
{
*softmax_cross_entropy_with_logits_sg/ShapeConst*
valueB"      *
dtype0*
_output_shapes
:
m
+softmax_cross_entropy_with_logits_sg/Rank_1Const*
value	B :*
dtype0*
_output_shapes
: 
}
,softmax_cross_entropy_with_logits_sg/Shape_1Const*
valueB"      *
dtype0*
_output_shapes
:
l
*softmax_cross_entropy_with_logits_sg/Sub/yConst*
dtype0*
_output_shapes
: *
value	B :
©
(softmax_cross_entropy_with_logits_sg/SubSub+softmax_cross_entropy_with_logits_sg/Rank_1*softmax_cross_entropy_with_logits_sg/Sub/y*
_output_shapes
: *
T0

0softmax_cross_entropy_with_logits_sg/Slice/beginPack(softmax_cross_entropy_with_logits_sg/Sub*
T0*

axis *
N*
_output_shapes
:
y
/softmax_cross_entropy_with_logits_sg/Slice/sizeConst*
valueB:*
dtype0*
_output_shapes
:
ö
*softmax_cross_entropy_with_logits_sg/SliceSlice,softmax_cross_entropy_with_logits_sg/Shape_10softmax_cross_entropy_with_logits_sg/Slice/begin/softmax_cross_entropy_with_logits_sg/Slice/size*
T0*
Index0*
_output_shapes
:

4softmax_cross_entropy_with_logits_sg/concat/values_0Const*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
r
0softmax_cross_entropy_with_logits_sg/concat/axisConst*
value	B : *
dtype0*
_output_shapes
: 

+softmax_cross_entropy_with_logits_sg/concatConcatV24softmax_cross_entropy_with_logits_sg/concat/values_0*softmax_cross_entropy_with_logits_sg/Slice0softmax_cross_entropy_with_logits_sg/concat/axis*
N*
_output_shapes
:*

Tidx0*
T0
¢
,softmax_cross_entropy_with_logits_sg/ReshapeReshapeadd_3+softmax_cross_entropy_with_logits_sg/concat*
T0*
Tshape0*
_output_shapes

:
m
+softmax_cross_entropy_with_logits_sg/Rank_2Const*
value	B :*
dtype0*
_output_shapes
: 
}
,softmax_cross_entropy_with_logits_sg/Shape_2Const*
valueB"      *
dtype0*
_output_shapes
:
n
,softmax_cross_entropy_with_logits_sg/Sub_1/yConst*
value	B :*
dtype0*
_output_shapes
: 
­
*softmax_cross_entropy_with_logits_sg/Sub_1Sub+softmax_cross_entropy_with_logits_sg/Rank_2,softmax_cross_entropy_with_logits_sg/Sub_1/y*
T0*
_output_shapes
: 
 
2softmax_cross_entropy_with_logits_sg/Slice_1/beginPack*softmax_cross_entropy_with_logits_sg/Sub_1*
T0*

axis *
N*
_output_shapes
:
{
1softmax_cross_entropy_with_logits_sg/Slice_1/sizeConst*
valueB:*
dtype0*
_output_shapes
:
ü
,softmax_cross_entropy_with_logits_sg/Slice_1Slice,softmax_cross_entropy_with_logits_sg/Shape_22softmax_cross_entropy_with_logits_sg/Slice_1/begin1softmax_cross_entropy_with_logits_sg/Slice_1/size*
_output_shapes
:*
T0*
Index0

6softmax_cross_entropy_with_logits_sg/concat_1/values_0Const*
valueB:
’’’’’’’’’*
dtype0*
_output_shapes
:
t
2softmax_cross_entropy_with_logits_sg/concat_1/axisConst*
value	B : *
dtype0*
_output_shapes
: 

-softmax_cross_entropy_with_logits_sg/concat_1ConcatV26softmax_cross_entropy_with_logits_sg/concat_1/values_0,softmax_cross_entropy_with_logits_sg/Slice_12softmax_cross_entropy_with_logits_sg/concat_1/axis*
T0*
N*
_output_shapes
:*

Tidx0
Ś
.softmax_cross_entropy_with_logits_sg/Reshape_1Reshape9softmax_cross_entropy_with_logits_sg/labels_stop_gradient-softmax_cross_entropy_with_logits_sg/concat_1*
T0*
Tshape0*
_output_shapes

:
Ņ
$softmax_cross_entropy_with_logits_sgSoftmaxCrossEntropyWithLogits,softmax_cross_entropy_with_logits_sg/Reshape.softmax_cross_entropy_with_logits_sg/Reshape_1*$
_output_shapes
::*
T0
n
,softmax_cross_entropy_with_logits_sg/Sub_2/yConst*
value	B :*
dtype0*
_output_shapes
: 
«
*softmax_cross_entropy_with_logits_sg/Sub_2Sub)softmax_cross_entropy_with_logits_sg/Rank,softmax_cross_entropy_with_logits_sg/Sub_2/y*
T0*
_output_shapes
: 
|
2softmax_cross_entropy_with_logits_sg/Slice_2/beginConst*
valueB: *
dtype0*
_output_shapes
:

1softmax_cross_entropy_with_logits_sg/Slice_2/sizePack*softmax_cross_entropy_with_logits_sg/Sub_2*
T0*

axis *
N*
_output_shapes
:
ś
,softmax_cross_entropy_with_logits_sg/Slice_2Slice*softmax_cross_entropy_with_logits_sg/Shape2softmax_cross_entropy_with_logits_sg/Slice_2/begin1softmax_cross_entropy_with_logits_sg/Slice_2/size*
T0*
Index0*
_output_shapes
:
Ą
.softmax_cross_entropy_with_logits_sg/Reshape_2Reshape$softmax_cross_entropy_with_logits_sg,softmax_cross_entropy_with_logits_sg/Slice_2*
T0*
Tshape0*
_output_shapes
:
O
ConstConst*
valueB: *
dtype0*
_output_shapes
:

MeanMean.softmax_cross_entropy_with_logits_sg/Reshape_2Const*
_output_shapes
: *

Tidx0*
	keep_dims( *
T0
Z
Variable_8/initial_valueConst*
value	B : *
dtype0*
_output_shapes
: 
n

Variable_8
VariableV2*
dtype0*
	container *
_output_shapes
: *
shape: *
shared_name 
Ŗ
Variable_8/AssignAssign
Variable_8Variable_8/initial_value*
use_locking(*
T0*
_class
loc:@Variable_8*
validate_shape(*
_output_shapes
: 
g
Variable_8/readIdentity
Variable_8*
T0*
_class
loc:@Variable_8*
_output_shapes
: 
c
ExponentialDecay/learning_rateConst*
valueB
 *ĶĢĢ<*
dtype0*
_output_shapes
: 
Z
ExponentialDecay/Cast/xConst*
value
B :Š*
dtype0*
_output_shapes
: 
v
ExponentialDecay/CastCastExponentialDecay/Cast/x*
Truncate( *

DstT0*
_output_shapes
: *

SrcT0
^
ExponentialDecay/Cast_1/xConst*
dtype0*
_output_shapes
: *
valueB
 *fff?
p
ExponentialDecay/Cast_2CastVariable_8/read*

SrcT0*
Truncate( *

DstT0*
_output_shapes
: 
t
ExponentialDecay/truedivRealDivExponentialDecay/Cast_2ExponentialDecay/Cast*
T0*
_output_shapes
: 
Z
ExponentialDecay/FloorFloorExponentialDecay/truediv*
T0*
_output_shapes
: 
o
ExponentialDecay/PowPowExponentialDecay/Cast_1/xExponentialDecay/Floor*
T0*
_output_shapes
: 
n
ExponentialDecayMulExponentialDecay/learning_rateExponentialDecay/Pow*
_output_shapes
: *
T0
R
gradients/ShapeConst*
valueB *
dtype0*
_output_shapes
: 
X
gradients/grad_ys_0Const*
valueB
 *  ?*
dtype0*
_output_shapes
: 
o
gradients/FillFillgradients/Shapegradients/grad_ys_0*
T0*

index_type0*
_output_shapes
: 
k
!gradients/Mean_grad/Reshape/shapeConst*
valueB:*
dtype0*
_output_shapes
:

gradients/Mean_grad/ReshapeReshapegradients/Fill!gradients/Mean_grad/Reshape/shape*
T0*
Tshape0*
_output_shapes
:
c
gradients/Mean_grad/ConstConst*
valueB:*
dtype0*
_output_shapes
:

gradients/Mean_grad/TileTilegradients/Mean_grad/Reshapegradients/Mean_grad/Const*

Tmultiples0*
T0*
_output_shapes
:
`
gradients/Mean_grad/Const_1Const*
valueB
 *  A*
dtype0*
_output_shapes
: 

gradients/Mean_grad/truedivRealDivgradients/Mean_grad/Tilegradients/Mean_grad/Const_1*
T0*
_output_shapes
:

Cgradients/softmax_cross_entropy_with_logits_sg/Reshape_2_grad/ShapeConst*
valueB:*
dtype0*
_output_shapes
:
å
Egradients/softmax_cross_entropy_with_logits_sg/Reshape_2_grad/ReshapeReshapegradients/Mean_grad/truedivCgradients/softmax_cross_entropy_with_logits_sg/Reshape_2_grad/Shape*
T0*
Tshape0*
_output_shapes
:
r
gradients/zeros_like	ZerosLike&softmax_cross_entropy_with_logits_sg:1*
_output_shapes

:*
T0

Bgradients/softmax_cross_entropy_with_logits_sg_grad/ExpandDims/dimConst*
valueB :
’’’’’’’’’*
dtype0*
_output_shapes
: 

>gradients/softmax_cross_entropy_with_logits_sg_grad/ExpandDims
ExpandDimsEgradients/softmax_cross_entropy_with_logits_sg/Reshape_2_grad/ReshapeBgradients/softmax_cross_entropy_with_logits_sg_grad/ExpandDims/dim*
T0*
_output_shapes

:*

Tdim0
Ļ
7gradients/softmax_cross_entropy_with_logits_sg_grad/mulMul>gradients/softmax_cross_entropy_with_logits_sg_grad/ExpandDims&softmax_cross_entropy_with_logits_sg:1*
_output_shapes

:*
T0
£
>gradients/softmax_cross_entropy_with_logits_sg_grad/LogSoftmax
LogSoftmax,softmax_cross_entropy_with_logits_sg/Reshape*
T0*
_output_shapes

:
§
7gradients/softmax_cross_entropy_with_logits_sg_grad/NegNeg>gradients/softmax_cross_entropy_with_logits_sg_grad/LogSoftmax*
_output_shapes

:*
T0

Dgradients/softmax_cross_entropy_with_logits_sg_grad/ExpandDims_1/dimConst*
valueB :
’’’’’’’’’*
dtype0*
_output_shapes
: 

@gradients/softmax_cross_entropy_with_logits_sg_grad/ExpandDims_1
ExpandDimsEgradients/softmax_cross_entropy_with_logits_sg/Reshape_2_grad/ReshapeDgradients/softmax_cross_entropy_with_logits_sg_grad/ExpandDims_1/dim*
T0*
_output_shapes

:*

Tdim0
ä
9gradients/softmax_cross_entropy_with_logits_sg_grad/mul_1Mul@gradients/softmax_cross_entropy_with_logits_sg_grad/ExpandDims_17gradients/softmax_cross_entropy_with_logits_sg_grad/Neg*
T0*
_output_shapes

:
Ā
Dgradients/softmax_cross_entropy_with_logits_sg_grad/tuple/group_depsNoOp8^gradients/softmax_cross_entropy_with_logits_sg_grad/mul:^gradients/softmax_cross_entropy_with_logits_sg_grad/mul_1
Ķ
Lgradients/softmax_cross_entropy_with_logits_sg_grad/tuple/control_dependencyIdentity7gradients/softmax_cross_entropy_with_logits_sg_grad/mulE^gradients/softmax_cross_entropy_with_logits_sg_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients/softmax_cross_entropy_with_logits_sg_grad/mul*
_output_shapes

:
Ó
Ngradients/softmax_cross_entropy_with_logits_sg_grad/tuple/control_dependency_1Identity9gradients/softmax_cross_entropy_with_logits_sg_grad/mul_1E^gradients/softmax_cross_entropy_with_logits_sg_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients/softmax_cross_entropy_with_logits_sg_grad/mul_1*
_output_shapes

:

Agradients/softmax_cross_entropy_with_logits_sg/Reshape_grad/ShapeConst*
dtype0*
_output_shapes
:*
valueB"      

Cgradients/softmax_cross_entropy_with_logits_sg/Reshape_grad/ReshapeReshapeLgradients/softmax_cross_entropy_with_logits_sg_grad/tuple/control_dependencyAgradients/softmax_cross_entropy_with_logits_sg/Reshape_grad/Shape*
T0*
Tshape0*
_output_shapes

:
k
gradients/add_3_grad/ShapeConst*
valueB"      *
dtype0*
_output_shapes
:
f
gradients/add_3_grad/Shape_1Const*
valueB:*
dtype0*
_output_shapes
:
ŗ
*gradients/add_3_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/add_3_grad/Shapegradients/add_3_grad/Shape_1*
T0*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’
Ö
gradients/add_3_grad/SumSumCgradients/softmax_cross_entropy_with_logits_sg/Reshape_grad/Reshape*gradients/add_3_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*
_output_shapes

:

gradients/add_3_grad/ReshapeReshapegradients/add_3_grad/Sumgradients/add_3_grad/Shape*
T0*
Tshape0*
_output_shapes

:
Ö
gradients/add_3_grad/Sum_1SumCgradients/softmax_cross_entropy_with_logits_sg/Reshape_grad/Reshape,gradients/add_3_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0*
_output_shapes
:

gradients/add_3_grad/Reshape_1Reshapegradients/add_3_grad/Sum_1gradients/add_3_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:
m
%gradients/add_3_grad/tuple/group_depsNoOp^gradients/add_3_grad/Reshape^gradients/add_3_grad/Reshape_1
Ł
-gradients/add_3_grad/tuple/control_dependencyIdentitygradients/add_3_grad/Reshape&^gradients/add_3_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/add_3_grad/Reshape*
_output_shapes

:
Ū
/gradients/add_3_grad/tuple/control_dependency_1Identitygradients/add_3_grad/Reshape_1&^gradients/add_3_grad/tuple/group_deps*
_output_shapes
:*
T0*1
_class'
%#loc:@gradients/add_3_grad/Reshape_1
·
gradients/MatMul_1_grad/MatMulMatMul-gradients/add_3_grad/tuple/control_dependencyVariable_6/read*
T0*
transpose_a( *
_output_shapes

: *
transpose_b(
°
 gradients/MatMul_1_grad/MatMul_1MatMulRelu_2-gradients/add_3_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(*
_output_shapes

: 
t
(gradients/MatMul_1_grad/tuple/group_depsNoOp^gradients/MatMul_1_grad/MatMul!^gradients/MatMul_1_grad/MatMul_1
ć
0gradients/MatMul_1_grad/tuple/control_dependencyIdentitygradients/MatMul_1_grad/MatMul)^gradients/MatMul_1_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/MatMul_1_grad/MatMul*
_output_shapes

: 
é
2gradients/MatMul_1_grad/tuple/control_dependency_1Identity gradients/MatMul_1_grad/MatMul_1)^gradients/MatMul_1_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients/MatMul_1_grad/MatMul_1*
_output_shapes

: 

gradients/Relu_2_grad/ReluGradReluGrad0gradients/MatMul_1_grad/tuple/control_dependencyRelu_2*
T0*
_output_shapes

: 
k
gradients/add_2_grad/ShapeConst*
dtype0*
_output_shapes
:*
valueB"       
f
gradients/add_2_grad/Shape_1Const*
dtype0*
_output_shapes
:*
valueB: 
ŗ
*gradients/add_2_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/add_2_grad/Shapegradients/add_2_grad/Shape_1*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’*
T0
±
gradients/add_2_grad/SumSumgradients/Relu_2_grad/ReluGrad*gradients/add_2_grad/BroadcastGradientArgs*
T0*
_output_shapes

: *

Tidx0*
	keep_dims( 

gradients/add_2_grad/ReshapeReshapegradients/add_2_grad/Sumgradients/add_2_grad/Shape*
T0*
Tshape0*
_output_shapes

: 
±
gradients/add_2_grad/Sum_1Sumgradients/Relu_2_grad/ReluGrad,gradients/add_2_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
: *

Tidx0*
	keep_dims( 

gradients/add_2_grad/Reshape_1Reshapegradients/add_2_grad/Sum_1gradients/add_2_grad/Shape_1*
_output_shapes
: *
T0*
Tshape0
m
%gradients/add_2_grad/tuple/group_depsNoOp^gradients/add_2_grad/Reshape^gradients/add_2_grad/Reshape_1
Ł
-gradients/add_2_grad/tuple/control_dependencyIdentitygradients/add_2_grad/Reshape&^gradients/add_2_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/add_2_grad/Reshape*
_output_shapes

: 
Ū
/gradients/add_2_grad/tuple/control_dependency_1Identitygradients/add_2_grad/Reshape_1&^gradients/add_2_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/add_2_grad/Reshape_1*
_output_shapes
: 
¶
gradients/MatMul_grad/MatMulMatMul-gradients/add_2_grad/tuple/control_dependencyVariable_4/read*
T0*
transpose_a( *
_output_shapes
:	*
transpose_b(
°
gradients/MatMul_grad/MatMul_1MatMulReshape-gradients/add_2_grad/tuple/control_dependency*
T0*
transpose_a(*
_output_shapes
:	 *
transpose_b( 
n
&gradients/MatMul_grad/tuple/group_depsNoOp^gradients/MatMul_grad/MatMul^gradients/MatMul_grad/MatMul_1
Ü
.gradients/MatMul_grad/tuple/control_dependencyIdentitygradients/MatMul_grad/MatMul'^gradients/MatMul_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/MatMul_grad/MatMul*
_output_shapes
:	
ā
0gradients/MatMul_grad/tuple/control_dependency_1Identitygradients/MatMul_grad/MatMul_1'^gradients/MatMul_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/MatMul_grad/MatMul_1*
_output_shapes
:	 
u
gradients/Reshape_grad/ShapeConst*%
valueB"            *
dtype0*
_output_shapes
:
¶
gradients/Reshape_grad/ReshapeReshape.gradients/MatMul_grad/tuple/control_dependencygradients/Reshape_grad/Shape*
T0*
Tshape0*&
_output_shapes
:

gradients/Relu_1_grad/ReluGradReluGradgradients/Reshape_grad/ReshapeRelu_1*
T0*&
_output_shapes
:
s
gradients/add_1_grad/ShapeConst*%
valueB"            *
dtype0*
_output_shapes
:
f
gradients/add_1_grad/Shape_1Const*
dtype0*
_output_shapes
:*
valueB:
ŗ
*gradients/add_1_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/add_1_grad/Shapegradients/add_1_grad/Shape_1*
T0*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’
µ
gradients/add_1_grad/SumSumgradients/Relu_1_grad/ReluGrad*gradients/add_1_grad/BroadcastGradientArgs*"
_output_shapes
:*

Tidx0*
	keep_dims( *
T0

gradients/add_1_grad/ReshapeReshapegradients/add_1_grad/Sumgradients/add_1_grad/Shape*&
_output_shapes
:*
T0*
Tshape0
±
gradients/add_1_grad/Sum_1Sumgradients/Relu_1_grad/ReluGrad,gradients/add_1_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 

gradients/add_1_grad/Reshape_1Reshapegradients/add_1_grad/Sum_1gradients/add_1_grad/Shape_1*
_output_shapes
:*
T0*
Tshape0
m
%gradients/add_1_grad/tuple/group_depsNoOp^gradients/add_1_grad/Reshape^gradients/add_1_grad/Reshape_1
į
-gradients/add_1_grad/tuple/control_dependencyIdentitygradients/add_1_grad/Reshape&^gradients/add_1_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/add_1_grad/Reshape*&
_output_shapes
:
Ū
/gradients/add_1_grad/tuple/control_dependency_1Identitygradients/add_1_grad/Reshape_1&^gradients/add_1_grad/tuple/group_deps*
_output_shapes
:*
T0*1
_class'
%#loc:@gradients/add_1_grad/Reshape_1
ś
$gradients/MaxPool_1_grad/MaxPoolGradMaxPoolGradConv2D_1	MaxPool_1-gradients/add_1_grad/tuple/control_dependency*
T0*
strides
*
data_formatNHWC*
ksize
*
paddingSAME*&
_output_shapes
:"

gradients/Conv2D_1_grad/ShapeNShapeNReluVariable_2/read*
N* 
_output_shapes
::*
T0*
out_type0
ø
+gradients/Conv2D_1_grad/Conv2DBackpropInputConv2DBackpropInputgradients/Conv2D_1_grad/ShapeNVariable_2/read$gradients/MaxPool_1_grad/MaxPoolGrad*
paddingSAME*&
_output_shapes
:"*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(
±
,gradients/Conv2D_1_grad/Conv2DBackpropFilterConv2DBackpropFilterRelu gradients/Conv2D_1_grad/ShapeN:1$gradients/MaxPool_1_grad/MaxPoolGrad*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingSAME*&
_output_shapes
:*
	dilations
*
T0

(gradients/Conv2D_1_grad/tuple/group_depsNoOp-^gradients/Conv2D_1_grad/Conv2DBackpropFilter,^gradients/Conv2D_1_grad/Conv2DBackpropInput

0gradients/Conv2D_1_grad/tuple/control_dependencyIdentity+gradients/Conv2D_1_grad/Conv2DBackpropInput)^gradients/Conv2D_1_grad/tuple/group_deps*
T0*>
_class4
20loc:@gradients/Conv2D_1_grad/Conv2DBackpropInput*&
_output_shapes
:"

2gradients/Conv2D_1_grad/tuple/control_dependency_1Identity,gradients/Conv2D_1_grad/Conv2DBackpropFilter)^gradients/Conv2D_1_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients/Conv2D_1_grad/Conv2DBackpropFilter*&
_output_shapes
:

gradients/Relu_grad/ReluGradReluGrad0gradients/Conv2D_1_grad/tuple/control_dependencyRelu*
T0*&
_output_shapes
:"
q
gradients/add_grad/ShapeConst*
dtype0*
_output_shapes
:*%
valueB"      "      
d
gradients/add_grad/Shape_1Const*
dtype0*
_output_shapes
:*
valueB:
“
(gradients/add_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/add_grad/Shapegradients/add_grad/Shape_1*2
_output_shapes 
:’’’’’’’’’:’’’’’’’’’*
T0
Æ
gradients/add_grad/SumSumgradients/Relu_grad/ReluGrad(gradients/add_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0*"
_output_shapes
:"

gradients/add_grad/ReshapeReshapegradients/add_grad/Sumgradients/add_grad/Shape*
T0*
Tshape0*&
_output_shapes
:"
«
gradients/add_grad/Sum_1Sumgradients/Relu_grad/ReluGrad*gradients/add_grad/BroadcastGradientArgs:1*
T0*
_output_shapes
:*

Tidx0*
	keep_dims( 

gradients/add_grad/Reshape_1Reshapegradients/add_grad/Sum_1gradients/add_grad/Shape_1*
T0*
Tshape0*
_output_shapes
:
g
#gradients/add_grad/tuple/group_depsNoOp^gradients/add_grad/Reshape^gradients/add_grad/Reshape_1
Ł
+gradients/add_grad/tuple/control_dependencyIdentitygradients/add_grad/Reshape$^gradients/add_grad/tuple/group_deps*
T0*-
_class#
!loc:@gradients/add_grad/Reshape*&
_output_shapes
:"
Ó
-gradients/add_grad/tuple/control_dependency_1Identitygradients/add_grad/Reshape_1$^gradients/add_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/add_grad/Reshape_1*
_output_shapes
:
ņ
"gradients/MaxPool_grad/MaxPoolGradMaxPoolGradConv2DMaxPool+gradients/add_grad/tuple/control_dependency*
T0*
strides
*
data_formatNHWC*
ksize
*
paddingSAME*&
_output_shapes
:C

gradients/Conv2D_grad/ShapeNShapeNtrain_data_phVariable/read*
T0*
out_type0*
N* 
_output_shapes
::
°
)gradients/Conv2D_grad/Conv2DBackpropInputConv2DBackpropInputgradients/Conv2D_grad/ShapeNVariable/read"gradients/MaxPool_grad/MaxPoolGrad*
	dilations
*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingSAME*&
_output_shapes
:C
“
*gradients/Conv2D_grad/Conv2DBackpropFilterConv2DBackpropFiltertrain_data_phgradients/Conv2D_grad/ShapeN:1"gradients/MaxPool_grad/MaxPoolGrad*&
_output_shapes
:*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(*
paddingSAME

&gradients/Conv2D_grad/tuple/group_depsNoOp+^gradients/Conv2D_grad/Conv2DBackpropFilter*^gradients/Conv2D_grad/Conv2DBackpropInput
ż
.gradients/Conv2D_grad/tuple/control_dependencyIdentity)gradients/Conv2D_grad/Conv2DBackpropInput'^gradients/Conv2D_grad/tuple/group_deps*&
_output_shapes
:C*
T0*<
_class2
0.loc:@gradients/Conv2D_grad/Conv2DBackpropInput

0gradients/Conv2D_grad/tuple/control_dependency_1Identity*gradients/Conv2D_grad/Conv2DBackpropFilter'^gradients/Conv2D_grad/tuple/group_deps*&
_output_shapes
:*
T0*=
_class3
1/loc:@gradients/Conv2D_grad/Conv2DBackpropFilter
ū
4GradientDescent/update_Variable/ApplyGradientDescentApplyGradientDescentVariableExponentialDecay0gradients/Conv2D_grad/tuple/control_dependency_1*
use_locking( *
T0*
_class
loc:@Variable*&
_output_shapes
:
ņ
6GradientDescent/update_Variable_1/ApplyGradientDescentApplyGradientDescent
Variable_1ExponentialDecay-gradients/add_grad/tuple/control_dependency_1*
use_locking( *
T0*
_class
loc:@Variable_1*
_output_shapes
:

6GradientDescent/update_Variable_2/ApplyGradientDescentApplyGradientDescent
Variable_2ExponentialDecay2gradients/Conv2D_1_grad/tuple/control_dependency_1*&
_output_shapes
:*
use_locking( *
T0*
_class
loc:@Variable_2
ō
6GradientDescent/update_Variable_3/ApplyGradientDescentApplyGradientDescent
Variable_3ExponentialDecay/gradients/add_1_grad/tuple/control_dependency_1*
T0*
_class
loc:@Variable_3*
_output_shapes
:*
use_locking( 
ś
6GradientDescent/update_Variable_4/ApplyGradientDescentApplyGradientDescent
Variable_4ExponentialDecay0gradients/MatMul_grad/tuple/control_dependency_1*
T0*
_class
loc:@Variable_4*
_output_shapes
:	 *
use_locking( 
ō
6GradientDescent/update_Variable_5/ApplyGradientDescentApplyGradientDescent
Variable_5ExponentialDecay/gradients/add_2_grad/tuple/control_dependency_1*
_output_shapes
: *
use_locking( *
T0*
_class
loc:@Variable_5
ū
6GradientDescent/update_Variable_6/ApplyGradientDescentApplyGradientDescent
Variable_6ExponentialDecay2gradients/MatMul_1_grad/tuple/control_dependency_1*
T0*
_class
loc:@Variable_6*
_output_shapes

: *
use_locking( 
ō
6GradientDescent/update_Variable_7/ApplyGradientDescentApplyGradientDescent
Variable_7ExponentialDecay/gradients/add_3_grad/tuple/control_dependency_1*
T0*
_class
loc:@Variable_7*
_output_shapes
:*
use_locking( 
ä
GradientDescent/updateNoOp5^GradientDescent/update_Variable/ApplyGradientDescent7^GradientDescent/update_Variable_1/ApplyGradientDescent7^GradientDescent/update_Variable_2/ApplyGradientDescent7^GradientDescent/update_Variable_3/ApplyGradientDescent7^GradientDescent/update_Variable_4/ApplyGradientDescent7^GradientDescent/update_Variable_5/ApplyGradientDescent7^GradientDescent/update_Variable_6/ApplyGradientDescent7^GradientDescent/update_Variable_7/ApplyGradientDescent

GradientDescent/valueConst^GradientDescent/update*
value	B :*
_class
loc:@Variable_8*
dtype0*
_output_shapes
: 

GradientDescent	AssignAdd
Variable_8GradientDescent/value*
T0*
_class
loc:@Variable_8*
_output_shapes
: *
use_locking( 
Ļ
Conv2D_2Conv2Dtrain_data_phVariable/read*
	dilations
*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingSAME*&
_output_shapes
:C
”
	MaxPool_2MaxPoolConv2D_2*
T0*
data_formatNHWC*
strides
*
ksize
*
paddingSAME*&
_output_shapes
:"
Y
add_4Add	MaxPool_2Variable_1/read*&
_output_shapes
:"*
T0
F
Relu_3Reluadd_4*
T0*&
_output_shapes
:"
Ź
Conv2D_3Conv2DRelu_3Variable_2/read*
	dilations
*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingSAME*&
_output_shapes
:"
”
	MaxPool_3MaxPoolConv2D_3*
ksize
*
paddingSAME*&
_output_shapes
:*
T0*
data_formatNHWC*
strides

Y
add_5Add	MaxPool_3Variable_3/read*
T0*&
_output_shapes
:
F
Relu_4Reluadd_5*
T0*&
_output_shapes
:
`
Shape_1Const*
dtype0*
_output_shapes
:*%
valueB"            
_
strided_slice_4/stackConst*
valueB: *
dtype0*
_output_shapes
:
a
strided_slice_4/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_4/stack_2Const*
dtype0*
_output_shapes
:*
valueB:

strided_slice_4StridedSliceShape_1strided_slice_4/stackstrided_slice_4/stack_1strided_slice_4/stack_2*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask
_
strided_slice_5/stackConst*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_5/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_5/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

strided_slice_5StridedSliceShape_1strided_slice_5/stackstrided_slice_5/stack_1strided_slice_5/stack_2*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0
_
strided_slice_6/stackConst*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_6/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_6/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

strided_slice_6StridedSliceShape_1strided_slice_6/stackstrided_slice_6/stack_1strided_slice_6/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: 
O
mul_2Mulstrided_slice_5strided_slice_6*
_output_shapes
: *
T0
_
strided_slice_7/stackConst*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_7/stack_1Const*
dtype0*
_output_shapes
:*
valueB:
a
strided_slice_7/stack_2Const*
dtype0*
_output_shapes
:*
valueB:

strided_slice_7StridedSliceShape_1strided_slice_7/stackstrided_slice_7/stack_1strided_slice_7/stack_2*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0*
shrink_axis_mask
E
mul_3Mulmul_2strided_slice_7*
T0*
_output_shapes
: 
i
Reshape_1/shapePackstrided_slice_4mul_3*
N*
_output_shapes
:*
T0*

axis 
e
	Reshape_1ReshapeRelu_4Reshape_1/shape*
_output_shapes
:	*
T0*
Tshape0
}
MatMul_2MatMul	Reshape_1Variable_4/read*
transpose_a( *
_output_shapes

: *
transpose_b( *
T0
P
add_6AddMatMul_2Variable_5/read*
T0*
_output_shapes

: 
>
Relu_5Reluadd_6*
T0*
_output_shapes

: 
z
MatMul_3MatMulRelu_5Variable_6/read*
T0*
transpose_a( *
_output_shapes

:*
transpose_b( 
P
add_7AddMatMul_3Variable_7/read*
T0*
_output_shapes

:
B
SoftmaxSoftmaxadd_7*
T0*
_output_shapes

:
×
Conv2D_4Conv2Dtest_data_phVariable/read*/
_output_shapes
:’’’’’’’’’C*
	dilations
*
T0*
strides
*
data_formatNHWC*
use_cudnn_on_gpu(*
paddingSAME
Ŗ
	MaxPool_4MaxPoolConv2D_4*/
_output_shapes
:’’’’’’’’’"*
T0*
data_formatNHWC*
strides
*
ksize
*
paddingSAME
b
add_8Add	MaxPool_4Variable_1/read*/
_output_shapes
:’’’’’’’’’"*
T0
O
Relu_6Reluadd_8*/
_output_shapes
:’’’’’’’’’"*
T0
Ó
Conv2D_5Conv2DRelu_6Variable_2/read*
paddingSAME*/
_output_shapes
:’’’’’’’’’"*
	dilations
*
T0*
data_formatNHWC*
strides
*
use_cudnn_on_gpu(
Ŗ
	MaxPool_5MaxPoolConv2D_5*
T0*
strides
*
data_formatNHWC*
ksize
*
paddingSAME*/
_output_shapes
:’’’’’’’’’
b
add_9Add	MaxPool_5Variable_3/read*
T0*/
_output_shapes
:’’’’’’’’’
O
Relu_7Reluadd_9*
T0*/
_output_shapes
:’’’’’’’’’
M
Shape_2ShapeRelu_7*
_output_shapes
:*
T0*
out_type0
_
strided_slice_8/stackConst*
valueB: *
dtype0*
_output_shapes
:
a
strided_slice_8/stack_1Const*
dtype0*
_output_shapes
:*
valueB:
a
strided_slice_8/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

strided_slice_8StridedSliceShape_2strided_slice_8/stackstrided_slice_8/stack_1strided_slice_8/stack_2*
end_mask *
_output_shapes
: *
Index0*
T0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask 
_
strided_slice_9/stackConst*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_9/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
a
strided_slice_9/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

strided_slice_9StridedSliceShape_2strided_slice_9/stackstrided_slice_9/stack_1strided_slice_9/stack_2*
shrink_axis_mask*
ellipsis_mask *

begin_mask *
new_axis_mask *
end_mask *
_output_shapes
: *
T0*
Index0
`
strided_slice_10/stackConst*
dtype0*
_output_shapes
:*
valueB:
b
strided_slice_10/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
b
strided_slice_10/stack_2Const*
valueB:*
dtype0*
_output_shapes
:

strided_slice_10StridedSliceShape_2strided_slice_10/stackstrided_slice_10/stack_1strided_slice_10/stack_2*
Index0*
T0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: 
P
mul_4Mulstrided_slice_9strided_slice_10*
T0*
_output_shapes
: 
`
strided_slice_11/stackConst*
valueB:*
dtype0*
_output_shapes
:
b
strided_slice_11/stack_1Const*
valueB:*
dtype0*
_output_shapes
:
b
strided_slice_11/stack_2Const*
dtype0*
_output_shapes
:*
valueB:

strided_slice_11StridedSliceShape_2strided_slice_11/stackstrided_slice_11/stack_1strided_slice_11/stack_2*
Index0*
T0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask *
_output_shapes
: 
F
mul_5Mulmul_4strided_slice_11*
T0*
_output_shapes
: 
i
Reshape_2/shapePackstrided_slice_8mul_5*
N*
_output_shapes
:*
T0*

axis 
v
	Reshape_2ReshapeRelu_7Reshape_2/shape*
T0*
Tshape0*0
_output_shapes
:’’’’’’’’’’’’’’’’’’

MatMul_4MatMul	Reshape_2Variable_4/read*
T0*
transpose_a( *'
_output_shapes
:’’’’’’’’’ *
transpose_b( 
Z
add_10AddMatMul_4Variable_5/read*
T0*'
_output_shapes
:’’’’’’’’’ 
H
Relu_8Reluadd_10*
T0*'
_output_shapes
:’’’’’’’’’ 

MatMul_5MatMulRelu_8Variable_6/read*
transpose_b( *
T0*
transpose_a( *'
_output_shapes
:’’’’’’’’’
Z
add_11AddMatMul_5Variable_7/read*'
_output_shapes
:’’’’’’’’’*
T0
N
	Softmax_1Softmaxadd_11*
T0*'
_output_shapes
:’’’’’’’’’
¾
initNoOp^Variable/Assign^Variable_1/Assign^Variable_2/Assign^Variable_3/Assign^Variable_4/Assign^Variable_5/Assign^Variable_6/Assign^Variable_7/Assign^Variable_8/Assign
P

save/ConstConst*
valueB Bmodel*
dtype0*
_output_shapes
: 

save/StringJoin/inputs_1Const*
dtype0*
_output_shapes
: *<
value3B1 B+_temp_1bc360b2741640d6862a0fee113781b8/part
u
save/StringJoin
StringJoin
save/Constsave/StringJoin/inputs_1*
N*
_output_shapes
: *
	separator 
Q
save/num_shardsConst*
value	B :*
dtype0*
_output_shapes
: 
\
save/ShardedFilename/shardConst*
value	B : *
dtype0*
_output_shapes
: 
}
save/ShardedFilenameShardedFilenamesave/StringJoinsave/ShardedFilename/shardsave/num_shards*
_output_shapes
: 
É
save/SaveV2/tensor_namesConst*}
valuetBr	BVariableB
Variable_1B
Variable_2B
Variable_3B
Variable_4B
Variable_5B
Variable_6B
Variable_7B
Variable_8*
dtype0*
_output_shapes
:	
u
save/SaveV2/shape_and_slicesConst*
dtype0*
_output_shapes
:	*%
valueB	B B B B B B B B B 
ę
save/SaveV2SaveV2save/ShardedFilenamesave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesVariable
Variable_1
Variable_2
Variable_3
Variable_4
Variable_5
Variable_6
Variable_7
Variable_8*
dtypes
2	

save/control_dependencyIdentitysave/ShardedFilename^save/SaveV2*
T0*'
_class
loc:@save/ShardedFilename*
_output_shapes
: 

+save/MergeV2Checkpoints/checkpoint_prefixesPacksave/ShardedFilename^save/control_dependency*
T0*

axis *
N*
_output_shapes
:
}
save/MergeV2CheckpointsMergeV2Checkpoints+save/MergeV2Checkpoints/checkpoint_prefixes
save/Const*
delete_old_dirs(
z
save/IdentityIdentity
save/Const^save/MergeV2Checkpoints^save/control_dependency*
T0*
_output_shapes
: 
Ģ
save/RestoreV2/tensor_namesConst*}
valuetBr	BVariableB
Variable_1B
Variable_2B
Variable_3B
Variable_4B
Variable_5B
Variable_6B
Variable_7B
Variable_8*
dtype0*
_output_shapes
:	
x
save/RestoreV2/shape_and_slicesConst*%
valueB	B B B B B B B B B *
dtype0*
_output_shapes
:	
ø
save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices*8
_output_shapes&
$:::::::::*
dtypes
2	
¦
save/AssignAssignVariablesave/RestoreV2*
T0*
_class
loc:@Variable*
validate_shape(*&
_output_shapes
:*
use_locking(
¢
save/Assign_1Assign
Variable_1save/RestoreV2:1*
use_locking(*
T0*
_class
loc:@Variable_1*
validate_shape(*
_output_shapes
:
®
save/Assign_2Assign
Variable_2save/RestoreV2:2*
validate_shape(*&
_output_shapes
:*
use_locking(*
T0*
_class
loc:@Variable_2
¢
save/Assign_3Assign
Variable_3save/RestoreV2:3*
validate_shape(*
_output_shapes
:*
use_locking(*
T0*
_class
loc:@Variable_3
§
save/Assign_4Assign
Variable_4save/RestoreV2:4*
use_locking(*
T0*
_class
loc:@Variable_4*
validate_shape(*
_output_shapes
:	 
¢
save/Assign_5Assign
Variable_5save/RestoreV2:5*
use_locking(*
T0*
_class
loc:@Variable_5*
validate_shape(*
_output_shapes
: 
¦
save/Assign_6Assign
Variable_6save/RestoreV2:6*
use_locking(*
T0*
_class
loc:@Variable_6*
validate_shape(*
_output_shapes

: 
¢
save/Assign_7Assign
Variable_7save/RestoreV2:7*
use_locking(*
T0*
_class
loc:@Variable_7*
validate_shape(*
_output_shapes
:

save/Assign_8Assign
Variable_8save/RestoreV2:8*
use_locking(*
T0*
_class
loc:@Variable_8*
validate_shape(*
_output_shapes
: 
Ø
save/restore_shardNoOp^save/Assign^save/Assign_1^save/Assign_2^save/Assign_3^save/Assign_4^save/Assign_5^save/Assign_6^save/Assign_7^save/Assign_8
-
save/restore_allNoOp^save/restore_shard "<
save/Const:0save/Identity:0save/restore_all (5 @F8"Ķ
	variablesæ¼
D

Variable:0Variable/AssignVariable/read:02truncated_normal:08
L
Variable_1:0Variable_1/AssignVariable_1/read:02truncated_normal_1:08
L
Variable_2:0Variable_2/AssignVariable_2/read:02truncated_normal_2:08
L
Variable_3:0Variable_3/AssignVariable_3/read:02truncated_normal_3:08
L
Variable_4:0Variable_4/AssignVariable_4/read:02truncated_normal_4:08
L
Variable_5:0Variable_5/AssignVariable_5/read:02truncated_normal_5:08
L
Variable_6:0Variable_6/AssignVariable_6/read:02truncated_normal_6:08
L
Variable_7:0Variable_7/AssignVariable_7/read:02truncated_normal_7:08
R
Variable_8:0Variable_8/AssignVariable_8/read:02Variable_8/initial_value:08"2
training&
$
GradientDescent
Mean:0
	Softmax:0"
train_op

GradientDescent"
testing

Softmax_1:0"×
trainable_variablesæ¼
D

Variable:0Variable/AssignVariable/read:02truncated_normal:08
L
Variable_1:0Variable_1/AssignVariable_1/read:02truncated_normal_1:08
L
Variable_2:0Variable_2/AssignVariable_2/read:02truncated_normal_2:08
L
Variable_3:0Variable_3/AssignVariable_3/read:02truncated_normal_3:08
L
Variable_4:0Variable_4/AssignVariable_4/read:02truncated_normal_4:08
L
Variable_5:0Variable_5/AssignVariable_5/read:02truncated_normal_5:08
L
Variable_6:0Variable_6/AssignVariable_6/read:02truncated_normal_6:08
L
Variable_7:0Variable_7/AssignVariable_7/read:02truncated_normal_7:08
R
Variable_8:0Variable_8/AssignVariable_8/read:02Variable_8/initial_value:08"X
placeholdersH
F
train_data_ph:0
train_labels_ph:0
test_data_ph:0
test_labels_ph:0*¤
serving_default
?
test_data_ph:0-
test_data_ph:0’’’’’’’’’C1
Softmax_1:0"
Softmax_1:0’’’’’’’’’tensorflow/serving/predict