function groups = get_channel_groups(dataset)

switch dataset
    
    case "dataset1" 
        % Neuroscan 64 electrodes
        frontal = unique([10, 9, 1, 20, 21, 14, 13, 12, 11, 2, 22, 23, 24, 25]);
        left = unique([18, 17, 16, 15, 36, 35, 34, 33, 40, 39, 38, 37]);
        right = unique([26, 27, 28, 29, 49, 50, 51, 52, 53, 54, 55, 56]);
        occipital = unique([42, 5, 58, 46, 6, 62, 48, 7, 64, 43, 59]);
    
    case "dataset2"
        % EGI 128 electrodes
        frontal = unique([26, 20, 17, 12, 7, 1, 91, ...
            21, 18, 14, 13, 8, 2, 92, ...
            19, 15, 9, 3, 93, ...
            16, 10, 4, 89]);
        left = unique([26, 27, 22, 23, 11, 6, 25, 24, 29, 28, 32, 33, 34, 30, 42, 41, 40, 37, 36, 35, 38, 39]);
        right = unique([91, 87, 88, 85, 84, 80, 79, 78, 83, 82, 77, 78, 79, 80, 61, 66, 70, 73, 76, 69, 72,75, 60, 65, 81]);
        occipital = unique([45, 46, 47, 48, 59, 64, 68, 71, 67, 63, 58, 49, 52, 51, 50, 53, 54, 55, 57, 56, 62]);

    otherwise
        error('Undefined dataset [%s]', dataset);
end

groups = struct('frontal', frontal, 'left', left, 'right', right, 'occipital', occipital);

%figure, scatter3(x,y,z)
%hold on
%scatter3(x(left),y(left),z(left),100,'r.')
%scatter3(x(right),y(right),z(right),100,'g.')
%scatter3(x(frontal),y(frontal),z(frontal),100,'b.')
%scatter3(x(occipital),y(occipital),z(occipital),100,'y.')

end