function create_dataset1()
% Add eeglab & fieldtrip to path
addpath(genpath('/home/aleksi/Workspace/software/eeglab14_1_0b'));
addpath('/home/aleksi/Workspace/software/fieldtrip-20181116');

% Remove octave pwelch from path
rmpath(genpath('/home/aleksi/Workspace/software/eeglab14_1_0b/functions/octavefunc'));

% Set output location
output_location = '/home/aleksi/Workspace/deep_learning/sleep_classification/preprocessed_data/dataset1';
if exist(output_location, 'dir') ~= 7
    mkdir(output_location);
    mkdir(fullfile(output_location), 'powers');
    mkdir(fullfile(output_location), 'labels');
end

% Get all .cnt files
dataset_location = '/home/aleksi/Workspace/deep_learning/sleep_classification/dataset1';
cnt_files = get_cnt_files(dataset_location);

% Load hori scores
hori = load(fullfile(dataset_location, 'Hori_data', 'horidata_ABC.mat'));

% Go through subjects and create power matrices
for file_idx = 1:numel(cnt_files)
    cnt_file = cnt_files(file_idx);
    
    % Get subject id from filename
    sub_id = extract_id(cnt_file);    
    
    % Read data
    eeg = pop_loadcnt(cnt_file);
    
    % Preprocess
    [eeg, removed_trials] = preprocess(eeg);
    
    %wpli = calculate_wpli(set_file);
    powers = calculate_powers(eeg, get_channel_groups('dataset1'));
    labels = extract_hori_scores(hori.horidataset, sub_id, removed_trials);
    
    % If number of labels doesn't match number of epochs don't use this
    % subject
    
    if size(powers,3) == numel(labels)
        save(fullfile(output_location, 'powers', sprintf('sub-%s', sub_id)), 'powers');
        save(fullfile(output_location, 'labels', sprintf('sub-%s', sub_id)), 'labels');
    end
end


end


function [eeg, removed_trials] = preprocess(eeg)

% Filter
eeg = pop_eegfiltnew(eeg, 1, 30, 1650);

% Resample
eeg = pop_resample(eeg, 250);

% Epoch
% First must transform string comments to chars otherwise pop_epoch crashes
eeg.comments = char(strjoin(eeg.comments));
eeg = pop_epoch(eeg, [], [-4, 0]);

% Remove bad channels
% Interpolate

% Remove bad trials
%[eeg, removed_trials] = pop_rejspec(eeg, 1, 'elecrange', 1:64, 'threshold', [-60, 60], 'freqlimits', [15,30], 'method','fft');
removed_trials = [];

end


function labels = extract_hori_scores(horidataset, sub_id, remove_trials)

if ~isempty(remove_trials)
    error('TODO');
end

%labels = cell2mat(horidataset.(['subj_' char(sub_id)]){1});
classes = string(horidataset.(['subj_' char(sub_id)]){4});
labels = nan(size(classes));
labels(classes=="Grapho"|classes=="Ripples") = 9;
labels(classes=="Alert") = 1;

end


function id = extract_id(filename)
split = strsplit(filename, '/');
split2 = strsplit(split(end), '.');
id = split2(1);
end


function cnt_files = get_cnt_files(dataset_location)
% Get all files in given location
files = dir(dataset_location);

% Remove folders
files([files.isdir]) = [];
files(1:2) = [];

% Get .set files
filenames = string({files.name});
set_file_idxs = endsWith(filenames, '.cnt');
cnt_files = fullfile(dataset_location, filenames(set_file_idxs))';

end