function create_training_sets()

dataset = "dataset2";

% Create a 3D CNN with previous epochs? Didn't really seem to help,
% probably best not to use
timelag = 0;

% Load powers into one huge array
path_powers = fullfile('/home/aleksi/Workspace/deep_learning/sleep_classification/preprocessed_data', dataset, 'powers');
dir_powers = dir(path_powers);
files = {dir_powers.name};
files([dir_powers.isdir]) = [];
files = sort_nat(files);

features = cell(numel(files),1);
for file_idx = 1:numel(files)
    dat = load(fullfile(path_powers, files{file_idx}));
    
    if timelag
        feat = zeros([timelag+1,size(dat.powers)]);
        for lag = 1:timelag+1
            feat(lag,:,:,:) = dat.powers(:,:,max((1:size(dat.powers,3))-lag-1,1));
        end
        features{file_idx} = feat(:,:,:,3:size(dat.powers,3));
    else
        features{file_idx} = dat.powers;
    end
end

% Concatenate data from all subjects and permute
if timelag
    features = permute(cat(4, features{:}), [2, 3, 1, 4]);
else
    features = permute(cat(3, features{:}), [1, 2, 4, 3]);
end

% Load labels into one huge vector
path_labels = fullfile('/home/aleksi/Workspace/deep_learning/sleep_classification/preprocessed_data', dataset, '/labels');
dir_labels = dir(path_labels);
files = {dir_labels.name};
files([dir_labels.isdir]) = [];
files = sort_nat(files);

labels = cell(numel(files),1);
for file_idx = 1:numel(files)
    dat = load(fullfile(path_labels, files{file_idx}));
    if timelag
        labels{file_idx} = dat.labels(3:end);
    else
        labels{file_idx} = dat.labels;
    end
end

% Get number of epochs for each subject
nlabels = cellfun(@(x) sum(~isnan(x)), labels);

% Concatenate labels
labels = cat(1, labels{:});
labels(ismember(labels,(1:2))) = 1;
labels(ismember(labels,(3:9))) = 2;

% Remove nan labels
retain = ~isnan(labels);
features = features(:,:,:,retain);
labels = labels(retain);

% Transform into one-hot vectors
labels_onehot = zeros(numel(labels),2);
labels_onehot(labels==1,1) = 1;
labels_onehot(labels==2,2) = 1;
labels_onehot = labels_onehot';

% Create train and test sets; only for dataset2

if dataset == "dataset2"
    %rng(5555);
    k = 5;
    cv = cvpartition(numel(files), 'kfold', k);
    folds = struct();
    for fold_idx = 1:k
        idxs = cv.training(fold_idx);
        train_indices = cell(numel(files),1);
        for sub_idx = 1:numel(nlabels)
            if idxs(sub_idx)
                c = ones(nlabels(sub_idx),1);
            else
                c = zeros(nlabels(sub_idx),1);
            end
            train_indices{sub_idx} = c;
        end

        train_indices = cat(1, train_indices{:});
        test_indices = ~train_indices;

        % Transform logical into indices with zero indexing (for python)
        folds(fold_idx).train_indices = find(train_indices)-1;
        folds(fold_idx).test_indices = find(test_indices)-1;

    end
end

savepath = fullfile('/home/aleksi/Workspace/deep_learning/sleep_classification/preprocessed_data/', dataset);

% Note: matlab files should be saved with param '-v7.3' so they can be read
% in python
if dataset == "dataset2"
    save(fullfile(savepath, 'all_data'), 'features', 'labels_onehot', 'folds', '-v7.3');
else
    save(fullfile(savepath, 'all_data'), 'features', 'labels_onehot', '-v7.3');
end
end