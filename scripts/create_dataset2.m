function create_dataset2()
% Add eeglab & fieldtrip to path
addpath(genpath('/home/aleksi/Workspace/software/eeglab14_1_0b'));
addpath('/home/aleksi/Workspace/software/fieldtrip-20181116');

% Remove octave pwelch from path
rmpath(genpath('/home/aleksi/Workspace/software/eeglab14_1_0b/functions/octavefunc'));

% Set output location
output_location = '/home/aleksi/Workspace/deep_learning/sleep_classification/preprocessed_data/dataset2';
if exist(output_location, 'dir') ~= 7
    mkdir(output_location);
    mkdir(fullfile(output_location), 'powers');
    mkdir(fullfile(output_location), 'labels');
end

% Get all eeglab .set files
dataset2_location = '/home/aleksi/Workspace/deep_learning/sleep_classification/dataset2';
set_files = get_set_files(dataset2_location);

% Load hori scores
hori = load(fullfile(dataset2_location, 'Hori_data', 'merged_AudMaskingCz_hori'));

% Go through subjects and create wpli matrices
for file_idx = 1:numel(set_files)
    set_file = set_files(file_idx);
    
    % Get subject id from filename
    sub_id = extract_id(set_file);
    
    % Load data
    eeg = pop_loadset(char(set_file));
    
    %wpli = calculate_wpli(set_file);
    powers = calculate_powers(eeg, get_channel_groups('dataset2'));
    labels = extract_hori_scores(hori.horidataset, sub_id);
    
    save(fullfile(output_location, 'powers', sprintf('sub-%s', sub_id)), 'powers');
    save(fullfile(output_location, 'labels', sprintf('sub-%s', sub_id)), 'labels');
end


end


function labels = extract_hori_scores(horidataset, sub_id)

epoch_idxs = horidataset.subj_id == str2double(sub_id);
labels = horidataset.HoridataV(epoch_idxs);

end


function id = extract_id(filename)
split = strsplit(filename, '/');
split2 = strsplit(split(8), '_');
id = split2(2);
end


function matrix = calculate_wpli(set_file)

% Read the file
eeg = pop_loadset(char(set_file));

% Calculate cross spectra
eeg = convert2ft(eeg);
cfg = [];
cfg.output = 'powandcsd';
cfg.method = 'mtmfft';
cfg.foilim = [0.5 45];
cfg.taper = 'dpss';
cfg.tapsmofrq = 0.3;
cfg.keeptrials = 'yes';
cfg.pad='nextpow2';
eeg = ft_freqanalysis(cfg,eeg);

% Calculate weighted phase lag index / coherence
matrix = zeros(2, numel(eeg.label), numel(eeg.label));
coh = zeros(numel(eeg.label), numel(eeg.label));

eeg.crsspctrm = eeg.crsspctrm(1,:,:);
wpli = ft_connectivity_wpli(eeg.crsspctrm,'debias',true,'dojack',false);

freqlist = [4, 8; 8, 12];
for f = 1:size(freqlist,1)
    [~, bstart] = min(abs(eeg.freq-freqlist(f,1)));
    [~, bend] = min(abs(eeg.freq-freqlist(f,2)));
    %[~,freqidx] = max(mean(wpli(:,bstart:bend),1));

    coh(:) = 0;
    coh(logical(tril(ones(size(coh)),-1))) = mean(wpli(:,bstart:bend), 2);
    coh = tril(coh,1)+tril(coh,1)';

    matrix(f,:,:) = coh;
end

end


function set_files = get_set_files(dataset_location)
% Get all files in given location
files = dir(dataset_location);

% Remove folders
files([files.isdir]) = [];
files(1:2) = [];

% Get .set files
filenames = string({files.name});
set_file_idxs = endsWith(filenames, '.set');
set_files = fullfile(dataset_location, filenames(set_file_idxs))';

end


function eeg = convert2ft(eeg)
eeg = eeglab2fieldtrip(eeg,'preprocessing','none');
eeg.elec.pnt = [-eeg.elec.pnt(:,2) eeg.elec.pnt(:,1) eeg.elec.pnt(:,3)];
eeg.elec.elecpos = eeg.elec.pnt;
eeg.elec.chanpos = eeg.elec.elecpos;
eeg.elec.unit = 'cm';
end