# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import numpy as np
import tensorflow as tf
from six.moves import range
from sklearn.metrics import recall_score, precision_score, f1_score
import json

import h5py


def accuracy(predictions, labels):
  return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
          / predictions.shape[0])


# Read train and test indices for each fold
def read_folds(file):
    nfolds = file["folds"]["train_indices"].shape[0]
    folds = dict()
    for fold_idx in range(nfolds):
        train_ref = file["folds"]["train_indices"][fold_idx][0]
        train_indices = np.squeeze(file[train_ref].value.astype(int))

        test_ref = file["folds"]["test_indices"][fold_idx][0]
        test_indices = np.squeeze(file[test_ref].value.astype(int))

        folds[fold_idx] = {'train_indices': train_indices, 'test_indices': test_indices}

    return folds


def main():
    # Load data
    data_file = '/home/aleksi/Workspace/deep_learning/sleep_classification/preprocessed_data/dataset2/all_data.mat'
    with h5py.File(data_file, 'r') as f:
        features = f["features"][:]
        labels = f["labels_onehot"][:]
        folds = read_folds(f)


    # Loop over folds
    accuracies = np.empty((len(folds.keys())))
    recall = np.empty((len(folds.keys()), 2))
    precision = np.empty((len(folds.keys()), 2))
    f1 = np.empty((len(folds.keys()), 2))

    # Do K-fold cross-validation
    for fold_idx in folds:

        # Create a net
        net = convnet(features.shape[1], features.shape[2], features.shape[3], labels.shape[1])

        # Get train and test data/labels
        train_dataset = features[folds[fold_idx]["train_indices"], :, :, :]
        train_labels = labels[folds[fold_idx]["train_indices"], :]
        test_dataset = features[folds[fold_idx]["test_indices"], :, :, :]
        test_labels = labels[folds[fold_idx]["test_indices"], :]

        # Do the training and testing with current fold
        [acc, recall_score, precision_score, f1_score] = train_and_test(net, train_dataset, train_labels, test_dataset, test_labels, fold_idx)
        #acc = train_and_test(net, features, labels, np.zeros((1, 1, 67, 4)), np.random.randint(10, size=(1, 2)))
        accuracies[fold_idx] = acc[-1]
        recall[fold_idx, :] = recall_score
        precision[fold_idx, :] = precision_score
        f1[fold_idx, :] = f1_score

    print(accuracies)


def reformat(labels):
    num_labels = len(np.unique(labels))
    labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
    return labels


def convnet(fxx1, fxx2, num_channels, num_labels):

    # Train with batches of 16
    batch_size = 16

    # Define dimensions for convolution
    patch_size_fxx2 = 5
    patch_size_fxx1 = fxx1
    depth = 8

    # Hidden layer size
    num_hidden = 32

    # Initialise the graph
    graph = tf.Graph()
    with graph.as_default():

        # Input data.
        tf_train_dataset = tf.placeholder(
            tf.float32, shape=(batch_size, fxx1, fxx2, num_channels), name="train_data_ph")
        tf_train_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels), name="train_labels_ph")
        tf_test_dataset = tf.placeholder(
            tf.float32, shape=(None, fxx1, fxx2, num_channels), name="test_data_ph")
        tf_test_labels = tf.placeholder(tf.float32, shape=(None, num_labels), name="test_labels_ph")

        # We're not doing validation
        #tf_valid_dataset = tf.constant(valid_dataset)
        #tf_test_dataset_size = tf.placeholder(tf.int16, shape=(1,), name="test_data_size_ph")

        # Variables that will be optimised during training
        layer1_weights = tf.Variable(tf.truncated_normal(
            [patch_size_fxx1, patch_size_fxx2, num_channels, depth], stddev=0.1))
        layer1_biases = tf.Variable(tf.truncated_normal([depth], stddev=0.1))
        layer2_weights = tf.Variable(tf.truncated_normal(
            [1, patch_size_fxx2, depth, depth], stddev=0.1))
        layer2_biases = tf.Variable(tf.truncated_normal([depth], stddev=0.1))
        layer3_weights = tf.Variable(tf.truncated_normal(
            [np.ceil(np.ceil((fxx2)/2)/2).astype(int) * depth, num_hidden], stddev=0.1))
        layer3_biases = tf.Variable(tf.truncated_normal([num_hidden], stddev=0.1))
        layer4_weights = tf.Variable(tf.truncated_normal(
            [num_hidden, num_labels], stddev=0.1))
        layer4_biases = tf.Variable(tf.truncated_normal([num_labels], stddev=0.1))

        # Model.
        def model(data):
            conv = tf.nn.conv2d(data, layer1_weights, [1, 3, 1, 1], padding='SAME')
            maxpool = tf.nn.max_pool(conv, [1, 1, 2, 1], [1, 1, 2, 1], padding='SAME')
            hidden = tf.nn.relu(maxpool + layer1_biases)
            conv = tf.nn.conv2d(hidden, layer2_weights, [1, 1, 1, 1], padding='SAME')
            maxpool = tf.nn.max_pool(conv, [1, 1, 2, 1], [1, 1, 2, 1], padding='SAME')
            hidden = tf.nn.relu(maxpool + layer2_biases)
            shape = tf.shape(hidden)
            reshape = tf.reshape(hidden, [shape[0], shape[1] * shape[2] * shape[3]])
            hidden = tf.matmul(reshape, layer3_weights) + layer3_biases
            hidden = tf.nn.relu(hidden)

            return tf.matmul(hidden, layer4_weights) + layer4_biases

        # Training computation.
        logits = model(tf_train_dataset)
        loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits))

        # Optimizer.
        global_step = tf.Variable(0)  # count the number of steps taken.
        learning_rate = tf.train.exponential_decay(0.025, global_step, 2000, 0.9, staircase=True)
        optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)

        # Predictions for the training, (validation), and test data.
        train_prediction = tf.nn.softmax(model(tf_train_dataset))
        #valid_prediction = tf.nn.softmax(model(tf_valid_dataset))
        test_prediction = tf.nn.softmax(model(tf_test_dataset))

    for node in (tf_train_dataset, tf_train_labels, tf_test_dataset, tf_test_labels):
        graph.add_to_collection("placeholders", node)

    for node in (optimizer, loss, train_prediction):
        graph.add_to_collection("training", node)

    graph.add_to_collection("testing", test_prediction)

    return graph


def train_and_test(graph, train_dataset, train_labels, test_dataset, test_labels, fold_idx):
    num_steps = 100001
    batch_size = 16
    nsamples = train_labels.shape[0]

    # Get nodes
    tf_train_dataset, tf_train_labels, tf_test_dataset, tf_test_labels \
        = graph.get_collection("placeholders")
    optimizer, loss, train_prediction = graph.get_collection("training")
    test_prediction = graph.get_collection("testing")[0]

    train_acc = []
    test_acc = []

    # Start training
    with tf.Session(graph=graph) as session:
        tf.global_variables_initializer().run()
        print('Initialized')
        for step in range(num_steps):
            # Choose a random batch of train data for training
            rand_idxs = np.random.choice(nsamples, batch_size, replace=False)
            batch_data = train_dataset[rand_idxs, :, :, :]
            batch_labels = train_labels[rand_idxs, :]

            # Do a training step
            feed_dict = {tf_train_dataset: batch_data, tf_train_labels: batch_labels}
            _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)

            if step % 50 == 0:
                # Predict with training and test data
                train_acc.append(accuracy(session.run(test_prediction,
                                          feed_dict={tf_test_dataset: train_dataset}), train_labels))
                test_acc.append(accuracy(session.run(test_prediction,
                                                     feed_dict={tf_test_dataset: test_dataset}), test_labels))

        # Predict with test data
        test_preds = np.argmax(session.run(test_prediction, feed_dict={tf_test_dataset: test_dataset}), 1)

        # Calculate recall, precision and f1 for test predictions
        recall = recall_score(np.argmax(test_labels, 1), test_preds, average=None)
        precision = precision_score(np.argmax(test_labels, 1), test_preds, average=None)
        f1 = f1_score(np.argmax(test_labels, 1), test_preds, average=None)

        # Save the model?
        if True:
            inputs = {"test_data_ph:0": tf_test_dataset}
            outputs = {"Softmax_1:0": test_prediction}
            save_path = "saved_models/fold"+str(fold_idx+1)
            tf.saved_model.simple_save(session, save_path, inputs, outputs)
            # Save corresponding recall, precision and f1 also
            prediction_stats = {"recall": recall.tolist(), "precision": precision.tolist(), "f1": f1.tolist()}
            with open(save_path + "/prediction_stats.json", "w") as outfile:
                json.dump(prediction_stats, outfile, sort_keys=True, indent=4, separators=(',', ': '))

    return test_acc, recall, precision, f1


main()