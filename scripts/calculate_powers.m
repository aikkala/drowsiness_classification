function powers = calculate_powers(eeg, channel_groups)

% Some params
nsamples = size(eeg.data, 2);
nchannels = size(eeg.data, 1);
ntrials = size(eeg.data,3);

% Get groups of channels
group_names = fieldnames(channel_groups);

% Use only frequencies up to 16Hz
[~, f] = pwelch(eeg.data(:,:,1)', nsamples, [], [], eeg.srate);
cutoff = 16;
[~, cutoff_idx] = min(abs(f-cutoff));

% Allocate array
powers = nan(numel(group_names), cutoff_idx, size(eeg.data,3));

% Loop over epochs
for epoch_idx = 1:ntrials
    power = pwelch(eeg.data(:,:,epoch_idx)', nsamples);
    
    % Randomise for testing purposes
    %X = randperm(numel(power));
    %power = reshape(power(X), size(power));

    % Create matrices
    total_power = 0;
    for group_idx = 1:numel(group_names)
        % Get mean PSD for each channel group
        group_power = median(power(:, channel_groups.(group_names{group_idx})),2);
        powers(group_idx,:,epoch_idx) = group_power(1:cutoff_idx);
        total_power = total_power + sum(group_power);
    end
    
    % Normalise by total power
    powers(:,:,epoch_idx) = powers(:,:,epoch_idx) ./ total_power;
end

end